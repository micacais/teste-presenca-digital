import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpClientModule } from '@angular/common/http'
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { AppService } from './app.service';
import { Observable } from 'rxjs/Observable';
import { AppRoutingModule } from './/app-routing.module';
import { ProgramasComponent } from './programas/programas.component';
import { ExponentialStrengthPipe } from './currency-br.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProgramasComponent,
    ExponentialStrengthPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [AppService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
