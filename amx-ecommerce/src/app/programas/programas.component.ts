import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit {

  programs: any;

  constructor(private appService: AppService) { }

  trackProgramId(index, program) {
    console.log(program.id_canal);
    return program ? program.id_canal : undefined;
  }

  ngOnInit() {
    this.appService.get('grade-canais.json').subscribe(res => {
      this.programs = res;
    });
  }

}
