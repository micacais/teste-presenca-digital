import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProgramasComponent } from './programas/programas.component';

const routes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'programas', component: ProgramasComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}